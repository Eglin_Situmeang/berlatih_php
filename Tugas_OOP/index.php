<?php
 require_once('animal.php');
 require_once('Ape.php');
 require_once('frog.php');

 $sheep = new Animal("Shaun");

 echo "Name: " .$sheep->name ."<br>";
 echo "legs: ".$sheep->legs ."<br>";
 echo "Cold blooded: ".$sheep->cold_blooded ."<br> <br>";
 
$Sungokong = new Ape ("Kera Sakti");
echo "Name: ".$Sungokong->name ."<br>";
echo "Legs: ".$Sungokong->legs ."<br>";
echo "Cold blooded: ".$Sungokong->cold_blooded ."<br>";
echo "Yell: ";
$Sungokong->yell();
echo "<br> <br>";
$kodok = new frog ("buduk");
echo "Name: ".$kodok->name ."<br>";
echo "Legs: ".$kodok->legs ."<br>";
echo "Cold blooded: ".$kodok->cold_blooded ."<br>";
echo "Jump: ";
$kodok->jump();